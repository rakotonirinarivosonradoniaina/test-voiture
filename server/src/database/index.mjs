import { MongoMemoryServer } from "mongodb-memory-server";
import { MongoClient } from "mongodb";

let database = null;

export const startDatabase = async () => {
  const mongo = await MongoMemoryServer.create();
  const uri = mongo.getUri();
  const connection = await MongoClient.connect(uri);
  database = connection.db();
};

export const getDatabase = async () => {
  if (!database) await startDatabase();
  return database;
};
