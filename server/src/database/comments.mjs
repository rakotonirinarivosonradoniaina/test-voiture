import { getDatabase } from "./index.mjs";

const collectionName = "comments";

export const insertComment = async (comment) => {
  const database = await getDatabase();
  const { insertedId } = await database.collection(collectionName)
    .insertOne(comment);
  return insertedId;
};

export const getComments = async (carId) => {
  const database = await getDatabase();
  return database.collection(collectionName)
    .find({ carId })
    .toArray();
};
