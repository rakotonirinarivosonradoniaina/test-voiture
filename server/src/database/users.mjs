import { getDatabase } from "./index.mjs";
import { ObjectID } from "mongodb";

const collectionName = "users";

export const signup = async (user) => {
  const database = await getDatabase();
  const { insertedId } = await database.collection(collectionName)
    .insertOne(user);
  return insertedId;
};

export const login = async (user) => {
  const database = await getDatabase();
  return database.collection(collectionName)
    .findOne({
      name: user.name,
      password: user.password
    });
};

export const getUser = async (id) => {
  const database = await getDatabase();
  return database.collection(collectionName)
    .findOne({ _id: ObjectID(id) });
};
