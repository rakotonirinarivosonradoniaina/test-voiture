import { getDatabase } from "./index.mjs";

const collectionName = "cars";

export const insertCar = async (car) => {
  const database = await getDatabase();
  const { insertedId } = await database.collection(collectionName)
    .insertOne(car);
  return insertedId;
};

export const getCars = async () => {
  const database = await getDatabase();
  const cars = await database.collection(collectionName)
    .find({})
    .toArray();
  return cars.splice(0, 10);
};
