import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import helmet from "helmet";
import morgan from "morgan";

import { startDatabase } from "./database/index.mjs";
import { getCars } from "./database/cars.mjs";
import { seedCars } from "./seed/index.mjs";
import { getComments, insertComment } from "./database/comments.mjs";
import { getUser, login, signup } from "./database/users.mjs";

const app = express();

app.use(helmet());
app.use(bodyParser.json());
app.use(cors());
app.use(morgan("combined"));

app.get("/cars", async (req, res) => {
  res.send(await getCars());
});

app.get("/comments/:carId", async (req, res) => {
  res.send(await getComments(req.params.carId));
});

app.get("/user/:id", async (req, res) => {
  res.send(await getUser(req.params.id));
});

app.post("/comment", async (req, res) => {
  const newComment = req.body;
  res.send(await insertComment(newComment));
});

app.post("/login", async (req, res) => {
  res.send(await login(req.body));
});

app.post("/signup", async (req, res) => {
  res.send(await signup(req.body));
});

startDatabase()
  .then(async () => {
    seedCars();
    app.listen(8080, () => {
      console.log("Server ready at http://localhost:8080");
    });
  })
  .catch(reason => console.log("start database error: ", reason));
