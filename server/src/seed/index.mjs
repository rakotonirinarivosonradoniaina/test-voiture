import fs from "fs";
import path from "path";
import csv from "fast-csv";
import { insertCar } from "../database/cars.mjs";
import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const CARS_INDEX = {
  model: 1,
  fullName: 2,
  imageUrl: 3,
  year: 4
};

export const seedCars = () => {
  console.log("seed cars...");
  fs.createReadStream(path.resolve(__dirname, "assets", "cars.csv"))
    .pipe(csv.parse({ headers: false }))
    .on("error", error => console.error(error))
    .on("data", async (row) => {
      const carInfo = row[0].split(";");
      const model = carInfo[CARS_INDEX.model];
      const fullName = carInfo[CARS_INDEX.fullName];
      const imageUrl = carInfo[CARS_INDEX.imageUrl];
      const year = carInfo[CARS_INDEX.year];
      await insertCar({
        model,
        fullName,
        imageUrl,
        year
      });
    })
    .on("end", rowCount => console.log(`Added ${rowCount} cars success`));
};
