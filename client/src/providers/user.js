import React from "react";

export const UserContext = React.createContext({
  user: null,
  setUser: () => {
  }
});

export const UserProvider = (props) => {
  const [user, setUser] = React.useState(null);
  return (
    <UserContext.Provider value={{
      user,
      setUser: (user) => {
        setUser(user);
      }
    }}>
      {props.children}
    </UserContext.Provider>
  );
};
