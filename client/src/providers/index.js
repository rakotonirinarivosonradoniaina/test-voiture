import React from "react";
import { UserProvider } from "./user";
import { MessageProvider } from "./message";

export const Providers = (props) => {
  return (
    <UserProvider>
      <MessageProvider>
        {props.children}
      </MessageProvider>
    </UserProvider>
  );
};
