import React from "react";
import { Alert, Snackbar } from "@mui/material";

export const MessageContext = React.createContext({
  displaySnackBar: () => {
  }
});

export const MessageProvider = (props) => {
  const [open, setOpen] = React.useState(false);
  const [message, setMessage] = React.useState("");
  const [severity, setSeverity] = React.useState("");
  return (
    <MessageContext.Provider value={{
      open,
      message,
      displaySnackBar: (input) => {
        setOpen(true);
        setMessage(input.message);
        setSeverity(input.severity);
      }
    }}>
      {props.children}
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "right"
        }}
        open={open}
        autoHideDuration={6000}
        onClose={() => {
          setOpen(false);
        }}>
        <Alert onClose={() => {
          setOpen(false);
        }} severity={severity}>{message}</Alert>
      </Snackbar>
    </MessageContext.Provider>
  );
};
