import React from "react";
import { createTheme } from "@mui/material";
import { ThemeProvider } from "@mui/styles";
import { Home } from "./pages/Home";
import { Providers } from "./providers";

const theme = createTheme();

function App() {

  return (
    <ThemeProvider theme={theme}>
      <Providers>
        <Home />
      </Providers>
    </ThemeProvider>
  );
}

export default App;
