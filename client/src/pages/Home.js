import React from "react";
import { Button, Container, Grid, Typography } from "@mui/material";
import axios from "axios";

import { CustomImageList } from "../components/CustomImageList";
import { CommentDialog } from "../components/commentDialog";
import { AuthentificationDialog } from "../components/AuthentificationDialog";
import { UserContext } from "../providers/user";

export const Home = () => {
  const {
    user,
    setUser
  } = React.useContext(UserContext);
  const [cars, setCars] = React.useState([]);
  const [selectedCar, setSelectedCar] = React.useState(null);
  const [openAuthenticationDialog, setOpenAuthenticationDialog] = React.useState(false);

  React.useEffect(() => {
    axios.get("http://localhost:8080/cars")
      .then(response => response.data)
      .then(data => {
        setCars(data.map(car => ({
          id: car._id,
          fullName: car.fullName,
          imageUrl: car.imageUrl
        })));
      })
      .catch(reason => console.log(reason));
  }, []);

  React.useEffect(() => {
    if (user) setOpenAuthenticationDialog(false);
  }, [user]);

  return (
    <Container>
      <Grid mt={2} mb={2} display="flex" alignItems="center" justifyContent="center">
        <Grid item={true} xs={6}>
          <Typography fontWeight="light" variant="h4">Liste des voitures</Typography>
        </Grid>
        <Grid item={true}>
          {!user &&
          <Button onClick={() => setOpenAuthenticationDialog(true)}>Se connecter</Button>}
          {user && <Button onClick={() => setUser(null)}>Se déconnecter</Button>}
        </Grid>
      </Grid>
      <Grid justifyContent="center" spacing={2} container={true}>
        <Grid item={true}>
          <CustomImageList items={cars} onItemSelected={(item) => setSelectedCar(item)} />
        </Grid>
      </Grid>
      <CommentDialog carInfo={selectedCar} open={!!selectedCar} onClose={() => setSelectedCar(null)} />
      <AuthentificationDialog open={openAuthenticationDialog} onClose={() => setOpenAuthenticationDialog(false)} />
    </Container>
  );
};
