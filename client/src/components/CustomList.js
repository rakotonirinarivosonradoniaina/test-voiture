import React from "react";
import {
  ListItem,
  List,
  Avatar,
  ListItemAvatar,
  ListItemText,
  ListSubheader,
  Divider, Box
} from "@mui/material";

export const CustomList = (props) => {
  const { comments } = props;
  return (
    <List subheader={
      <ListSubheader component="div" id="nested-list-subheader">
        {comments.length} Commentaires
      </ListSubheader>
    }>
      {comments.length ? (
        comments.map((comment, index) => (
          <Box key={comment.id}>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  {comment.user.name.substring(0, 1)}
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={comment.user.name} secondary={comment.description} />
            </ListItem>
            {index < comments.length - 1 && <Divider />}
          </Box>
        ))
      ) : null}
    </List>
  );
};
