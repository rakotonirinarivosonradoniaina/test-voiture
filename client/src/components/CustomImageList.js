import React from "react";
import { IconButton, ImageList, ImageListItem, ImageListItemBar, Typography } from "@mui/material";
import InserComment from "@mui/icons-material/InsertComment";
import { UserContext } from "../providers/user";

export const CustomImageList = (props) => {
  const {
    items,
    onItemSelected
  } = props;
  const { user } = React.useContext(UserContext);
  return (
    <ImageList sx={{
      width: 500,
      height: 600
    }}>
      {items.map((item) => (<ImageListItem key={item.id}>
        <img
          src={item.imageUrl}
          alt={item.fullName}
          loading="lazy"
        />
        <ImageListItemBar
          title={<Typography variant="body2" fontWeight="bold">{item.fullName}</Typography>}
          actionIcon={
            <IconButton
              disabled={!user}
              onClick={() => onItemSelected(item)}
              sx={{ color: "rgba(255, 255, 255, 0.54)" }}
              aria-label={`info about ${item.title}`}
            >
              <InserComment />
            </IconButton>
          }
        />
      </ImageListItem>))}
    </ImageList>
  );
};
