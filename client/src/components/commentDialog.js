import React from "react";
import { Dialog, DialogActions, DialogContent, DialogTitle, IconButton } from "@mui/material";
import { CustomTextField } from "./CustomTextField";
import { CustomList } from "./CustomList";
import CloseIcon from "@mui/icons-material/Close";
import axios from "axios";
import { UserContext } from "../providers/user";

export const CommentDialog = (props) => {
  const {
    open,
    onClose,
    carInfo
  } = props;

  const { user } = React.useContext(UserContext);

  const [comments, setComments] = React.useState([]);
  const [newComment, setNewComment] = React.useState();

  React.useEffect(() => {
    if (carInfo)
      axios.get("http://localhost:8080/comments/" + carInfo.id)
        .then(response => response.data)
        .then(data => {
          if (data) {
            setComments(data.map(comment => ({
              id: comment._id,
              description: comment.description,
              user: {
                id: comment.user.id,
                name: comment.user.name
              }
            })));
          }
        })
        .catch(reason => console.log(reason));
  }, [carInfo]);

  const handleClick = () => {
    if (newComment)
      axios.post("http://localhost:8080/comment", {
        carId: carInfo.id,
        description: newComment,
        user: {
          id: user.id,
          name: user.name
        }
      })
        .then(response => response)
        .then(data => {
          if (data) {
            setComments(prevState => [...prevState, {
              id: data,
              description: newComment,
              user: {
                id: user.id,
                name: user.name
              }
            }]);
            setNewComment("");
          }
        })
        .catch(reason => console.log(reason));
  };

  const handleChange = (event) => {
    setNewComment(event.target.value);
  };

  return (
    <Dialog onClose={onClose} scroll={"paper"} open={open}>
      <DialogTitle>
        <img
          alt={carInfo?.fullName}
          width={400}
          src={carInfo?.imageUrl} />
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500]
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <CustomList comments={comments} />
      </DialogContent>
      <DialogActions>
        <CustomTextField value={newComment} onChange={handleChange} onSend={handleClick} />
      </DialogActions>
    </Dialog>
  );
};
