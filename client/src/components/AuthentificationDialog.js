import React from "react";
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton, Link,
  TextField,
  Typography
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import axios from "axios";
import { UserContext } from "../providers/user";
import { MessageContext } from "../providers/message";

export const AuthentificationDialog = (props) => {
  const {
    onClose,
    open
  } = props;

  const { displaySnackBar } = React.useContext(MessageContext);

  const [state, setState] = React.useState({
    name: "",
    password: ""
  });
  const [authentificationType, setAuthentificationType] = React.useState("login");
  const { setUser } = React.useContext(UserContext);

  const handleClikAuthentification = () => {
    setAuthentificationType(authentificationType === "login" ? "signup" : "login");
  };

  const handleClick = () => {
    if (state.name && state.password)
      axios.post("http://localhost:8080/" + authentificationType, {
        name: state.name,
        password: state.password
      })
        .then(response => response.data)
        .then(data => {
          if (data) {
            if (authentificationType === "login") {
              setUser(data);
              setState({
                name: "",
                password: ""
              });
              displaySnackBar({
                message: "Bienvenue",
                severity: "success"
              });
            } else {
              setAuthentificationType("login");
              displaySnackBar({
                message: "Inscription réussi",
                severity: "success"
              });
            }
          } else {
            displaySnackBar({
              message: "Mot de passe ou nom incorrect",
              severity: "error"
            });
          }
        });
  };

  const handleChange = (e, type) => {
    setState(prevState => ({
      ...prevState,
      [type]: e.target.value
    }));
  };

  return (
    <Dialog onClose={onClose} scroll={"paper"} open={open}>
      <DialogTitle>
        <Typography pt={4} textAlign="center" variant="h4" fontWeight="light">
          {authentificationType ? "Connexion" : "Inscription"}
        </Typography>
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500]
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <Box my={2} width={400}>
          <TextField
            value={state.name}
            onChange={(e) => handleChange(e, "name")}
            fullWidth={true}
            variant="outlined"
            label="Nom" />
        </Box>
        <Box my={2} width={400}>
          <TextField
            type="password"
            value={state.password}
            onChange={(e) => handleChange(e, "password")}
            fullWidth={true}
            variant="outlined"
            label="Mot de passe" />
        </Box>
        <Box mt={4}>
          <Button onClick={handleClick} variant="outlined" fullWidth={true}>
            {authentificationType === "login" ? "Se connecter" : "S'inscrire"}
          </Button>
        </Box>
        <Typography mt={3}>
          {authentificationType === "login" ? "Vous n'avez pas encore de compte ? " : "ou "}
          <Link onClick={handleClikAuthentification}
                href="#">
            {authentificationType === "login" ? "s'inscrire" : "se connecter"}
          </Link>
        </Typography>

      </DialogContent>
    </Dialog>
  );
};
