import React from "react";
import { FormControl, IconButton, InputAdornment, OutlinedInput } from "@mui/material";
import { makeStyles, createStyles } from "@mui/styles";
import { grey } from "@mui/material/colors";
import SendIcon from "@mui/icons-material/Send";

export const CustomTextField = (props) => {
  const {
    value,
    onSend,
    onChange
  } = props;
  const classes = useStyles();
  return (
    <FormControl sx={{
      m: 1,
      width: "100%"
    }} variant="outlined">
      <OutlinedInput
        fullWidth={true}
        className={classes.root}
        size="small"
        placeholder="commentaire..."
        value={value}
        onChange={onChange}
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              aria-label="toggle password visibility"
              onClick={onSend}
              edge="end"
            >
              <SendIcon />
            </IconButton>
          </InputAdornment>
        }
      />
    </FormControl>
  );
};

const useStyles = makeStyles(() => createStyles({
  root: {
    backgroundColor: grey[200],
    borderWidth: 0
  }
}));
